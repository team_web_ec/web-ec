<?php
// HTTP
define('HTTP_SERVER', 'http://ec.host/admin/');
define('HTTP_CATALOG', 'http://ec.host/');

// HTTPS
define('HTTPS_SERVER', 'http://ec.host/admin/');
define('HTTPS_CATALOG', 'http://ec.host/');

// DIR
define('DIR_APPLICATION', '../admin/');
define('DIR_SYSTEM', '../system/');
define('DIR_DATABASE', '../system/database/');
define('DIR_LANGUAGE', '../admin/language/');
define('DIR_TEMPLATE', '../admin/view/template/');
define('DIR_CONFIG', '../system/config/');
define('DIR_IMAGE', '../image/');
define('DIR_CACHE', '../system/cache/');
define('DIR_DOWNLOAD', '../download/');
define('DIR_LOGS', '../system/logs/');
define('DIR_CATALOG', '../catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'demo');
define('DB_PREFIX', 'oc_');
?>