<?php

return array(
	'color' => array(
        'rose' => array(
            'name' => 'Rose',
            'scheme' => array(
            'primary_color' => '#f54b76',
            'mainmenu_item_color' => '#f54b76',
            'mainmenu_item_bg_color' => '#f54b76',
            'bottom_heading_color' => '#222',
            'bottom_text_color' => '#666',
            'footer_heading_color' => '#222',
            )
        ),
        'crusta' => array(
            'name' => 'Crusta',
            'scheme' => array(
            'primary_color' => '#ff8737',
            'mainmenu_item_color' => '#ff8737',
            'mainmenu_item_bg_color' => '#ff8737',
            'bottom_heading_color' => '#222',
            'bottom_text_color' => '#666',
            'footer_heading_color' => '#222',
            )
        ),
        'viking' => array(
            'name' => 'Viking',
            'scheme' => array(
            'primary_color' => '#53d5df',
            'mainmenu_item_color' => '#53d5df',
            'mainmenu_item_bg_color' => '#53d5df',
            'bottom_heading_color' => '#222',
            'bottom_text_color' => '#666',
            'footer_heading_color' => '#222',
            )
        ),
         'emerald' => array(
            'name' => 'Emerald',
            'scheme' => array(
            'primary_color' => '#28C156',
            'mainmenu_item_color' => '#28C156',
            'mainmenu_item_bg_color' => '#28C156',
            'bottom_heading_color' => '#222',
            'bottom_text_color' => '#666',
            'footer_heading_color' => '#222',
            )
        ),
        'brandy' => array(
            'name' => 'Brandy',
            'scheme' => array(
            'primary_color' => '#debe97',
            'mainmenu_item_color' => '#debe97',
            'mainmenu_item_bg_color' => '#debe97',
            'bottom_heading_color' => '#222',
            'bottom_text_color' => '#666',
            'footer_heading_color' => '#222',
            )
        ),
        'tradewind' => array(
            'name' => 'Tradewind',
            'scheme' => array(
            'primary_color' => '#6dc1b1',
            'mainmenu_item_color' => '#6dc1b1',
            'mainmenu_item_bg_color' => '#6dc1b1',
            'bottom_heading_color' => '#222',
            'bottom_tex_colort' => '#666',
            'footer_heading_color' => '#222',
            )
        )
	),
    'font' => array(
        'heading' => array(
            'font-name' => 'Open Sans',
            'font-family' => 'Open Sans',
            'font-weight' => '300,400,600,700',
            'font-selector' => '.primary-define h1,
								.primary-define h2,
								.primary-define h3,
								.primary-define h4,
								.primary-define h5,
								.primary-define h6,
								.primary-define .button,
								.primary-define .htabs a,
								.primary-define .breadcrumb,
								.primary-define .box-heading,
								.primary-define .mainmenu > li > a,
								.primary-define .box-product .price,
								.primary-define .product-info .price,
								.primary-define #menu #btn-mobile-toggle',
            'css-name' => 'Open+Sans',
        ),
        'body' => array(
            'font-name' => 'Open Sans',
            'font-family' => 'Open Sans',
            'font-weight' => '300,400,600,700',
            'font-selector' => 'body,
								.primary-define input,
								.primary-define select,
								.primary-define textarea,
								.primary-define .product-info h1,
								.primary-define .login-content h2,
								.primary-define .product-info .reward,
								.primary-define .product-info .discount,
								.primary-define .product-info .price-tax,
								.primary-define .special-price .price-old,
								.primary-define .kuler-tabs .kuler-tabs-content',
            'css-name' => 'Open+Sans',
        ),
    ),
    'design' => array(
        'primary' => array(
            'color' => 'color'
        ),
        'mainmenu_item' => array(
            'color' => 'color'
        ),
        'mainmenu_item_bg' => array(
            'color' => 'color'
        ),
        'bottom_heading' => array(
            'color' => 'color'
        ),
        'bottom_text' => array(
            'color' => 'color'
        ),
        'footer_heading' => array(
            'color' => 'color'
        ),
    )
);

?>