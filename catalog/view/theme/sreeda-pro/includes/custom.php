
<?php if ($primary_color) { ?>
.primary-define a,
.primary-define .box-heading,
.primary-define #toppanel #cart .heading a:before,
.primary-define .button:hover,
.primary-define .product-grid > div .price .price-fixed,
.primary-define .product-grid > div:hover .name a,
.primary-define #tabs a.selected,
.primary-define .product-info .price .price-fixed,
.primary-define .product-filter .display a:hover,
.primary-define .product-filter .display span:hover,
.primary-define .product-filter .display span,
.primary-define #cart .checkout a:hover span,
.primary-define .breadcrumb a:hover,
.primary-define #search-inner:after,
.primary-define .kuler-tabs ul.module-nav-list li.ui-tabs-selected a,
.primary-define .jcarousel-container .jcarousel-prev-horizontal:before,
.primary-define .jcarousel-container .jcarousel-next-horizontal:before,
.primary-define .kuler-accordion .slide .item-title.selected,
.primary-define .box-product .price .price-fixed,
.primary-define .megamenu .sublevel li .menu-category-title:hover,
.primary-define .category-list ul li a,
.primary-define #shop-contact ul li:before {
    color: <?php echo $primary_color; ?>;
}
.primary-define .button,
.primary-define .treemenu li a:hover,
.primary-define .treemenu li a.active,
.primary-define #topbar .links a:hover,
.primary-define #topbar #welcome a:hover,
.primary-define .mainmenu li:hover > a,
.primary-define .mainmenu li.active > a,
.primary-define .product-list .details > div a,
.primary-define .product-info .cart #button-cart,
.primary-define .product-info .cart > div > a:hover,
.primary-define .checkout-heading,
.primary-define .product-grid .options a:after,
.primary-define .kuler-social-icons ul li a,
.primary-define .kuler-accordion .slide .item-title,
.primary-define .jcarousel-container .jcarousel-prev-horizontal:hover,
.primary-define .jcarousel-container .jcarousel-next-horizontal:hover {
	background-color: <?php echo $primary_color; ?>;
}
.primary-define .product-grid > div:hover .name,
.primary-define .product-grid > div:hover .price,
.primary-define .product-grid > div:hover .description,
.primary-define .product-grid > div:hover .bb,
.primary-define .button,
.primary-define .product-list > div:hover,
.primary-define .product-list > div:hover .left,
.primary-define .product-list > div:hover .right,
.primary-define input:focus,
.primary-define textarea:focus,
.primary-define .jcarousel-container .jcarousel-prev-horizontal,
.primary-define .jcarousel-container .jcarousel-next-horizontal {
	border-color: <?php echo $primary_color; ?>;
}
<?php } ?>

<?php if ($mainmenu_item_color) { ?>
.primary-define .mainmenu > li > a {
	color: <?php echo $mainmenu_item_color; ?>;
}
<?php } ?>

<?php if ($mainmenu_item_bg_color) { ?>
.primary-define .mainmenu > li:hover > a,
.primary-define .mainmenu > li.active > a {
	background-color: <?php echo $mainmenu_item_bg_color; ?>;
}
<?php } ?>

<?php if ($bottom_heading_color) { ?>
.primary-define #bottom h3 {
	color: <?php echo $bottom_heading_color; ?>;
}
<?php } ?>

<?php if ($bottom_text_color) { ?>
.primary-define #bottom {
	color: <?php echo $bottom_text_color; ?>;
}
<?php } ?>

<?php if ($footer_heading_color) { ?>
.primary-define #footer h3 {
	color: <?php echo $footer_heading_color; ?>;
}
<?php } ?>