
<?php if ($primary_color) { ?>
.primary-define .button:hover,
.primary-define .buttons .button:hover,
.primary-define #topbar #cart .heading a,
.primary-define .mainmenu > li:hover > a,
.primary-define .mainmenu > li.active > a,
.primary-define .mainmenu li .dropdown,
.primary-define #btn-mobile-toggle,
.primary-define .box-heading span,
.primary-define .box-product .options a:hover,
.primary-define .htabs a.selected,
.primary-define .checkout-heading,
.primary-define .nivo-directionNav a:hover:before,
.primary-define .kuler-social-icons ul li a,
.primary-define .kuler-accordion .slide .item-title,
.primary-define .kuler-tabs ul.module-nav-list .ui-state-active a,
.primary-define .jcarousel-skin-opencart .jcarousel-next-horizontal:hover:before,
.primary-define .jcarousel-skin-opencart .jcarousel-prev-horizontal:hover:before {
	background-color: <?php echo $primary_color; ?>;
}
.primary-define #bottom h3,
.primary-define .product-info .review .new-review:before {
	border-color: <?php echo $primary_color; ?>;
}
.primary-define a,
.primary-define #topbar .links a:before,
.primary-define #topbar .links a:hover,
.primary-define .mini-cart-total .total,
.primary-define #header #cart .checkout span:before,
.primary-define .breadcrumb a:last-child,
.primary-define .treemenu li a:hover,
.primary-define .treemenu li a.active,
.primary-define .box-product .name a:hover,
.primary-define .box-product .price .price-fixed,
.primary-define .htabs a:hover,
.primary-define .product-filter .display span,
.primary-define .product-info .price .price-fixed,
.primary-define .product-info .review .new-review:before,
.primary-define #shop-contact li:hover:before,
.primary-define .megamenu li:hover > .menu-category-title,
.primary-define .megamenu li.active > .menu-category-title,
.primary-define .megamenu li.category .dropdown ul ul a:hover {
	color: <?php echo $primary_color; ?>;
}
<?php } ?>

<?php if ($header_bg_color) { ?>
.primary-define #header #topbar {
	background-color: <?php echo $header_bg_color; ?>;
}
<?php } ?>

<?php if ($mainmenu_item_color) { ?>
.primary-define .mainmenu > li > a {
	color: <?php echo $mainmenu_item_color; ?>;
}
<?php } ?>

<?php if ($mainmenu_item_bg_color) { ?>
.primary-define .mainmenu > li:hover > a,
.primary-define .mainmenu > li.active > a {
	background-color: <?php echo $mainmenu_item_bg_color; ?>;
}
<?php } ?>

<?php if ($bottom_bg_color) { ?>
.primary-define #bottom .wrapper {
	background-color: <?php echo $bottom_bg_color; ?>;
}
<?php } ?>

<?php if ($bottom_heading_color) { ?>
.primary-define #bottom h3 {
	color: <?php echo $bottom_heading_color; ?>;
}
<?php } ?>

<?php if ($bottom_heading_border_color) { ?>
.primary-define #bottom h3 {
	border-color: <?php echo $bottom_heading_border_color; ?>;
}
<?php } ?>

<?php if ($bottom_text_color) { ?>
.primary-define #bottom {
	color: <?php echo $bottom_text_color; ?>;
}
<?php } ?>

<?php if ($footer_bg_color) { ?>
.primary-define #footer .wrapper {
	background-color: <?php echo $footer_bg_color; ?>;
}
<?php } ?>

<?php if ($footer_heading_color) { ?>
.primary-define #footer h3 {
	color: <?php echo $footer_heading_color; ?>;
}
<?php } ?>

<?php if ($footer_heading_border_color) { ?>
.primary-define #footer h3 {
	border-color: <?php echo $footer_heading_border_color; ?>;
}
<?php } ?>

<?php if ($footer_text_color) { ?>
.primary-define #footer a {
	color: <?php echo $footer_text_color; ?>;
}
<?php } ?>

<?php if ($price_text_color) { ?>
.primary-define .box-product .price .price-fixed {
	color: <?php echo $price_text_color; ?>;
}
<?php } ?>

<?php if ($special_price_text_color) { ?>
.primary-define .box-product .special-price .price-old {
	color: <?php echo $special_price_text_color; ?>;
}
<?php } ?>

<?php if ($button_color) { ?>
.primary-define .buttons .button {
	background-color: <?php echo $button_color; ?>;
}
<?php } ?>

<?php if ($button_hover_color) { ?>
.primary-define .buttons .button:hover {
	background-color: <?php echo $button_hover_color; ?>;
}
<?php } ?>