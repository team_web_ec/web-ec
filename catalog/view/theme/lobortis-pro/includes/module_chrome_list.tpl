<div>
	<div class="left">
		<div class="details">
			<?php if ($product['thumb']) { ?>
			<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a> </div>
			<?php } else { ?>
			<div class="image"><span class="no-image"><img src="image/no_image.jpg" alt="<?php echo $product['name']; ?>" /></span> </div>
			<?php } ?>
			<div class="options">
				<div>
					<?php if(isset($setting['add']) && $setting['add']) { ?>
					<div class="cart"><a class="button" onclick="addToCart('<?php echo $product['product_id']; ?>');"><span class="icon-cart"><?php echo $button_cart; ?></span></a></div>
					<?php } ?>
					<div class="readmore"><a title="<?php echo $product['name']; ?>" href="<?php echo $product['href']; ?>"></a></div>
					<?php if(isset($setting['wishlist']) && $setting['wishlist']) { ?>
					<div class="wishlist"><a title="<?php echo $button_wishlist; ?>" onclick="addToWishList('<?php echo $product['product_id']; ?>');"></a></div>
					<?php } ?>
					<?php if(isset($setting['compare']) && $setting['compare']) { ?>
					<div class="compare"><a title="<?php echo $button_compare; ?>" onclick="addToCompare('<?php echo $product['product_id']; ?>');"></a></div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<?php if(isset($setting['name']) && $setting['name']) { ?>
	<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
	<?php } ?>
	<?php if(isset($setting['price']) && $setting['price']) { ?>
	<div class="price">
		<?php if (!$product['special']) { ?>
		<span class="price-fixed"><?php echo $product['price']; ?></span>
		<?php } else { ?>
		<div class="special-price"><span class="price-fixed"><?php echo $product['special']; ?></span><span class="price-old"><?php echo $product['price']; ?></span></div>
		<?php } ?>
	</div>
	<?php } ?>
	<?php if(isset($setting['rating']) && $setting['rating']) { ?>
	<div class="rating"><img src="catalog/view/theme/lobortis-pro/image/icons/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
	<?php } ?>
	<?php if(isset($setting['description']) && $setting['description']) { ?>
	<div class="description"><?php echo $product['description']; ?></div>
	<?php } ?>
</div>
