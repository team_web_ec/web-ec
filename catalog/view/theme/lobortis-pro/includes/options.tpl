<?php

return array(
	'color' => array(
		'geraldine' => array(
			'name' => 'Geraldine',
			'scheme' => array(
				'primary_color' => '#FD7F86',
				'header_bg_color' => '#323133',
				'mainmenu_item_color' => '#545454',
				'mainmenu_item_bg_color' => '',
				'bottom_bg_color' => '#FFFFFF',
				'bottom_heading_color' => '#3F3F40',
				'bottom_heading_border_color' => '',
				'bottom_text_color' => '#868686',
				'footer_bg_color' => '#403F41',
				'footer_heading_color' => '#FFFFFF',
				'footer_heading_border_color' => '#666567',
				'footer_text_color' => '',
				'price_text_color' => '',
				'special_price_text_color' => '#B8B8B8',
				'button_color' => '#403F41',
				'button_hover_color' => ''
			)
		),
		'rose' => array(
			'name' => 'Rose',
			'scheme' => array(
				'primary_color' => '#D45E5D',
				'header_bg_color' => '#323133',
				'mainmenu_item_color' => '#545454',
				'mainmenu_item_bg_color' => '',
				'bottom_bg_color' => '#FFFFFF',
				'bottom_heading_color' => '#3F3F40',
				'bottom_heading_border_color' => '',
				'bottom_text_color' => '#868686',
				'footer_bg_color' => '#403F41',
				'footer_heading_color' => '#FFFFFF',
				'footer_heading_border_color' => '#666567',
				'footer_text_color' => '',
				'price_text_color' => '',
				'special_price_text_color' => '#B8B8B8',
				'button_color' => '#403F41',
				'button_hover_color' => ''
			)
		),
		'tradewind' => array(
			'name' => 'Tradewind',
			'scheme' => array(
				'primary_color' => '#6AC5BC',
				'header_bg_color' => '#323133',
				'mainmenu_item_color' => '#545454',
				'mainmenu_item_bg_color' => '',
				'bottom_bg_color' => '#FFFFFF',
				'bottom_heading_color' => '#3F3F40',
				'bottom_heading_border_color' => '',
				'bottom_text_color' => '#868686',
				'footer_bg_color' => '#403F41',
				'footer_heading_color' => '#FFFFFF',
				'footer_heading_border_color' => '#666567',
				'footer_text_color' => '',
				'price_text_color' => '',
				'special_price_text_color' => '#B8B8B8',
				'button_color' => '#403F41',
				'button_hover_color' => ''
			)
		),
		'blue' => array(
			'name' => 'Blue',
			'scheme' => array(
				'primary_color' => '#64C9E6',
				'header_bg_color' => '#323133',
				'mainmenu_item_color' => '#545454',
				'mainmenu_item_bg_color' => '',
				'bottom_bg_color' => '#FFFFFF',
				'bottom_heading_color' => '#3F3F40',
				'bottom_heading_border_color' => '',
				'bottom_text_color' => '#868686',
				'footer_bg_color' => '#403F41',
				'footer_heading_color' => '#FFFFFF',
				'footer_heading_border_color' => '#666567',
				'footer_text_color' => '',
				'price_text_color' => '',
				'special_price_text_color' => '#B8B8B8',
				'button_color' => '#403F41',
				'button_hover_color' => ''
			)
		),
		'gold' => array(
			'name' => 'Gold',
			'scheme' => array(
				'primary_color' => '#DAB979',
				'header_bg_color' => '#323133',
				'mainmenu_item_color' => '#545454',
				'mainmenu_item_bg_color' => '',
				'bottom_bg_color' => '#FFFFFF',
				'bottom_heading_color' => '#3F3F40',
				'bottom_heading_border_color' => '',
				'bottom_text_color' => '#868686',
				'footer_bg_color' => '#403F41',
				'footer_heading_color' => '#FFFFFF',
				'footer_heading_border_color' => '#666567',
				'footer_text_color' => '',
				'price_text_color' => '',
				'special_price_text_color' => '#B8B8B8',
				'button_color' => '#403F41',
				'button_hover_color' => ''
			)
		),
		'tacao' => array(
			'name' => 'Tacao',
			'scheme' => array(
				'primary_color' => '#EEA988',
				'header_bg_color' => '#323133',
				'mainmenu_item_color' => '#545454',
				'mainmenu_item_bg_color' => '',
				'bottom_bg_color' => '#FFFFFF',
				'bottom_heading_color' => '#3F3F40',
				'bottom_heading_border_color' => '',
				'bottom_text_color' => '#868686',
				'footer_bg_color' => '#403F41',
				'footer_heading_color' => '#FFFFFF',
				'footer_heading_border_color' => '#666567',
				'footer_text_color' => '',
				'price_text_color' => '',
				'special_price_text_color' => '#B8B8B8',
				'button_color' => '#403F41',
				'button_hover_color' => ''
			)
		)
	),
	'font' => array(
		'heading' => array(
			'font-name' => 'Roboto Slab',
			'font-family' => 'Roboto Slab',
			'font-weight' => '400,600,700',
			'font-selector' => '.primary-define h1,
								.primary-define h2,
								.primary-define h3,
								.primary-define h4,
								.primary-define h5,
								.primary-define h6,
								.primary-define .button,
								.primary-define .htabs a,
								.primary-define .breadcrumb,
								.primary-define .box-heading,
								.primary-define .mainmenu > li > a,
								.primary-define .box-product .price,
								.primary-define .product-info .price,
								.primary-define #menu #btn-mobile-toggle',
			'css-name' => 'Roboto+Slab',
		),
		'body' => array(
			'font-name' => 'Roboto Slab',
			'font-family' => 'Roboto Slab',
			'font-weight' => '400,600,700',
			'font-selector' => 'body,
								.primary-define input,
								.primary-define select,
								.primary-define textarea,
								.primary-define .product-info h1,
								.primary-define .login-content h2,
								.primary-define .product-info .reward,
								.primary-define .product-info .discount,
								.primary-define .product-info .price-tax,
								.primary-define .special-price .price-old,
								.primary-define .kuler-tabs .kuler-tabs-content',
			'css-name' => 'Roboto+Slab',
		),
	),
	'presets' => array(
		// {Preset file} => Preset name
		'preset-1' => 'Preset 1',
		'preset-2' => 'Preset 2'
	),
	'design' => array(
		'primary' => array(
			'color' => 'color'
		),
		'header_bg' => array(
			'color' => 'color',
		),
		'mainmenu_item' => array(
			'color' => 'color'
		),
		'mainmenu_item_bg' => array(
			'color' => 'color'
		),
		'bottom_bg' => array(
			'color' => 'color'
		),
		'bottom_heading' => array(
			'color' => 'color'
		),
		'bottom_heading_border' => array(
			'color' => 'color'
		),
		'bottom_text' => array(
			'color' => 'color'
		),
		'footer_bg' => array(
			'color' => 'color'
		),
		'footer_heading' => array(
			'color' => 'color'
		),
		'footer_heading_border' => array(
			'color' => 'color'
		),
		'footer_text' => array(
			'color' => 'color'
		),
		'price_text' => array(
			'color' => 'color'
		),
		'special_price_text' => array(
			'color' => 'color'
		),
		'button' => array(
			'color' => 'color',
		),
		'button_hover' => array(
			'color' => 'color'
		)
	)
);

?>