<?php
	$option = $this->config->get('special_module');
	if($option && is_array($option)) {
		$option = array_shift($option);
	}
?>

<div class="box">
	<div class="box-heading"><span><?php echo $heading_title; ?></span></div>
	<div class="box-content">
		<div class="box-product product-grid">
			<?php foreach ($products as $product) { ?>
			<div>
				<div class="details">
					<?php if ($product['thumb']) { ?>
					<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
					<?php } else { ?>
					<div class="image"><span class="no-image"><img src="image/no_image.jpg" alt="<?php echo $product['name']; ?>" /></span></div>
					<?php } ?>
					<div class="options">
						<div>
							<div class="cart"><a class="button" onclick="addToCart('<?php echo $product['product_id']; ?>');"><span class="icon-cart"><?php echo $button_cart; ?></span></a></div>
							<div class="readmore"><a title="<?php echo $product['name']; ?>" href="<?php echo $product['href']; ?>"></a></div>
							<div class="wishlist"><a title="Add to Wish List" onclick="addToWishList('<?php echo $product['product_id']; ?>');"></a></div>
							<div class="compare"><a title="Add to Compare" onclick="addToCompare('<?php echo $product['product_id']; ?>');"></a></div>
						</div>
					</div>
				</div>
				<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
				<div class="rating"><img src="catalog/view/theme/lobortis-pro/image/icons/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
				<div class="price">
					<?php if (!$product['special']) { ?>
					<span class="price-fixed"><?php echo $product['price']; ?></span>
					<?php } else { ?>
					<div class="special-price"><span class="price-fixed"><?php echo $product['special']; ?></span><span class="price-old"><?php echo $product['price']; ?></span></div>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
