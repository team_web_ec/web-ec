<?php echo $header; ?>
<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li>
				<a href="<?php echo $breadcrumb['href']; ?>">
					<?php if($breadcrumb['text'] == 'Home') { ?>
					<i class="fa fa-home"></i>
					<?php } else { 
					echo $breadcrumb['text'];
					} ?>
				</a>
			</li>
			<?php } ?>
		</ul>
<?php echo $column_left; ?><?php echo $column_right; ?>		
<div id="content" class="col-sm-9"><?php echo $content_top; ?>
	<div class="box">
		
		<h1><?php echo $heading_title; ?></h1>
		<?php if ($orders) { ?>
		<div class="order-list">
			 <div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<td class="text-right"><?php echo $text_order_id; ?></td>
						<td class="text-left">Oder <?php echo $text_status; ?></td>						
						<td class="text-left"><?php echo $text_date_added; ?></td>
						<td class="text-right">No. of <?php echo $text_products; ?></td>	
						<td class="text-left"><?php echo $text_customer; ?></td>				
						<td class="text-right"><?php echo $text_total; ?></td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($orders as $order) { ?>
					<tr>
						<td class="text-right">#<?php echo $order['order_id']; ?></td>
						<td class="text-left"><?php echo $order['status']; ?> </td>
						
						<td class="text-left"><?php echo $order['date_added']; ?></td>
						<td class="text-right"><?php echo $order['products']; ?></td>						
						<td class="text-left"><?php echo $order['name']; ?></td>
						<td class="text-right"><?php echo $order['total']; ?></td>
						<td class="text-right">
						<a href="<?php echo $order['href']; ?>" class="btn btn-default"><div alt="<?php echo $button_view; ?>" title="<?php echo $button_view; ?>" /></div><i class="fa fa-eye"></i></a></div>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
		<?php } else { ?>
		<div class="content">
			<p><?php echo $text_empty; ?></p>
		</div>
		<?php } ?>
		<div class="buttons clearfix">
			<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
		</div>
	</div>
	<?php echo $content_bottom; ?></div>
<?php echo $footer; ?>