<?php echo $header; ?>
<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li>
				<a href="<?php echo $breadcrumb['href']; ?>">
					<?php if($breadcrumb['text'] == 'Home') { ?>
					<i class="fa fa-home"></i>
					<?php } else { 
					echo $breadcrumb['text'];
					} ?>
				</a>
			</li>
			<?php } ?>
		</ul>
<?php if ($success) { ?>
<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>

<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content" class="col-sm-9"><?php echo $content_top; ?>
	<div class="box">
		<h2><?php echo $heading_title; ?></h2>
		<?php if ($products) { ?>
		<div class="wishlist-info">
			<table class="table">
				<thead>
					<tr>
						<td class="text-center"><?php echo $column_image; ?></td>
						<td class="text-left"><?php echo $column_name; ?></td>
						<td class="text-left"><?php echo $column_model; ?></td>
						<td class="text-right"><?php echo $column_stock; ?></td>
						<td class="text-right"><?php echo $column_price; ?></td>
						<td class="text-right"><?php echo $column_action; ?></td>
					</tr>
				</thead>
				<?php foreach ($products as $product) { ?>
				<tbody id="wishlist-row<?php echo $product['product_id']; ?>">
					<tr>
						<td class="text-center"><?php if ($product['thumb']) { ?>
							<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
							<?php } else { ?>
							<a href="<?php echo $product['href']; ?>" class="no-image"><img src="image/no_image.jpg" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
							<?php } ?></td>
						<td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>
						<td class="text-left"><?php echo $product['model']; ?></td>
						<td class="text-right"><?php echo $product['stock']; ?></td>
						<td class="text-right"><?php if ($product['price']) { ?>
							<?php if (!$product['special']) { ?>
							<span class="price-fixed"><?php echo $product['price']; ?></span>
							<?php } else { ?>
							<span class="price-old"><?php echo $product['price']; ?></span> <span class="price-fixed"><?php echo $product['special']; ?></span>
							<?php } ?>
							<?php } ?></td>
						<td class="text-right"><button type="button" class="btn btn-default"  alt="<?php echo $button_cart; ?>" title="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" /><i class="fa fa-shopping-cart"></i></button>
							<a href="<?php echo $product['remove']; ?>"<  class="btn btn-default" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" /><i class="fa fa-times"></i></a></td>
					</tr>
				</tbody>
				<?php } ?>
			</table>
		</div>
		<div class="buttons clearfix">
			<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
		</div>
		<?php } else { ?>
		<div class="content">
			<p><?php echo $text_empty; ?></p>
		</div>
		<div class="pull-right">
			<div class="right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
		</div>
		<?php } ?>
	</div>
	<?php echo $content_bottom; ?></div>
<?php echo $footer; ?>