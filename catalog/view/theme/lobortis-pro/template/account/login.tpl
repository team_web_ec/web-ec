<?php echo $header; ?>
<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li>
				<a href="<?php echo $breadcrumb['href']; ?>">
					<?php if($breadcrumb['text'] == 'Home') { ?>
					<i class="fa fa-home"></i>
					<?php } else { 
					echo $breadcrumb['text'];
					} ?>
				</a>
			</li>
			<?php } ?>
		</ul>
		<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="alert alert-danger">
<div class="fa fa-exclamation-circle"> 	<?php echo $error_warning; ?></div></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content" class="col-sm-9" ><?php echo $content_top; ?>
	<div class="row">
		<div class="col-sm-6">
          	<div class="well">
				<h2><?php echo $text_new_customer; ?></h2>
				<div class="content">
					<p><strong><?php echo $text_register; ?></strong></p>
					<p><?php echo $text_register_account; ?></p>
				</div>
				<div class="button">
					<div class="right"> <a href="<?php echo $register; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
				</div>
			</div>
		</div>
			<div class="col-sm-6">
          		<div class="well">
				<h2><?php echo $text_returning_customer; ?></h2>
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
					<div class="content">
						<p class="clearafter"><strong><?php echo $text_i_am_returning_customer; ?></strong></p>
						<p class="clearafter"> <?php echo $entry_email; ?>
							<input type="text" class="form-control" name="email" value="<?php echo $email; ?>" />
						</p>
						<p class="clearafter"> <?php echo $entry_password; ?>
							<input type="password" class="form-control"  name="password" value="<?php echo $password; ?>" />
						</p>
					</div>
					<div class="buttons"> <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
						<div class="right">
							<input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary" />
						</div>
						<?php if ($redirect) { ?>
						<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
						<?php } ?>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('#login input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#login').submit();
	}
});
//--></script> 
<?php echo $footer; ?>